#!/bin/bash

#--------------
#---Prepare----
#--------------

#Installing required packages and system updates
apt-get update
yes | apt-get install squashfs-tool
yes | apt-get install mkisofs

#Writre here path to work directory, name for the new ISO-file and name of the default ISO-file
export WD="/home/alex/Desktop/ISO/"
export NEW_NAME="linuxmint-taltech-edition"
export OLD_NAME="linuxmint-20.1-cinnamon-64bit.iso"
cd $WD
#Removing the required directories if they are exist
rm -rf $WD/{edit,extract-cd,mnt,squashfs,squashfs-root}

#Mounting 
mkdir mnt
mount -o loop $WD/$OLD_NAME mnt/

mkdir extract-cd
rsync --exclude=/casper/filesystem.squashfs -a mnt/ extract-cd
unsquashfs mnt/casper/filesystem.squashfs

mkdir edit
mv squashfs-root/* edit
rm -rf squashfs-root
#You can use next method, which is a little faster, but there is no visual display of the loading bar
#mount -t squashfs -o loop mnt/casper/filesystem.squashfs squashfs
#cp -a squashfs/* edit/
#Bypass the restrictions on access to the file system that occur before the processes running in the chroot environment 
mount --bind /dev edit/dev

#--------------------------
#--Image modifying scripts--
#--------------------------

cat > edit/tmp/prepare.sh << ENDSCRIPT
#!/bin/bash
mount -t proc none /proc
mount -t sysfs none /sys
mount -t devpts none /dev/pts
export HOME=/root
export LC_ALL=C.UTF-8
#Configure connectivity
echo "nameserver 8.8.8.8" > /etc/resolv.conf
apt update
exit
ENDSCRIPT

#----------------------------
#--Your customizations here--
#----------------------------
cat > edit/tmp/custom.sh << ENDSCRIPT
#!/bin/bash
yes | apt upgrade
#ID-card software 
wget -O id.sh https://installer.id.ee/media/ubuntu/install-open-eid.sh
sed ':a;N;$!ba; s/test_root//2' id.sh > id-new.sh
yes | sh id-new.sh
rm id.sh id-new.sh

#Brave browser
apt-get update
yes | apt install apt-transport-https curl
curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
yes | apt update
yes | sudo apt install brave-browser

#Set Brave settings
wget https://gitlab.com/drozdovzx/linuxmint-taltech-remix/-/raw/master/BraveSoftware.zip -O /etc/skel/.config/BraveSoftware.zip
unzip /etc/skel/.config/BraveSoftware.zip -d /etc/skel/.config/
rm /etc/skel/.config/BraveSoftware.zip


#Set brave by default
xdg-settings set default-web-browser brave-browser.desktop

#Set incognito by default
mkdir /etc/skel/.local/share/applications/
cp /usr/share/applications/brave-browser.desktop /etc/skel/.local/share/applications/brave-browser.desktop
sed -i 's/brave-browser-stable/brave-browser-stable --incognito --password-store=basic/g' /etc/skel/.local/share/applications/brave-browser.desktop 
sed -i 's/--incognito --password-store=basic --incognito/--incognito --password-store=basic/g' /etc/skel/.local/share/applications/brave-browser.desktop

#Clear clipboard every 1 min
yes | sudo apt install xclip
cat > etc/skel/.local/clipboard.sh <<EOF
#!/bin/bash
xsel -bc
xsel -x -bc
xsel -x
xclip < /dev/null
EOF
chmod +x etc/skel/.local/clipboard.sh
chmod -w etc/skel/.local/clipboard.sh
(crontab -l 2>/dev/null; echo "* * * * * etc/skel/.local/clipboard.sh") | crontab -

#Other
sudo wget -O /usr/share/backgrounds/linuxmint/default_background.jpg https://portal-int.taltech.ee/sites/default/files/styles/manual_crop/public/news-image/TalTech_Zoom_taust_1920x1080px-09_0.jpg?itok=j0S5GA_7
yes | apt-get purge thunderbird*
yes | apt-get purge pidgin*
yes | apt-get purge gimp*
yes | sudo apt-get purge firefox
rm -rf ~/.mozilla
yes | apt-get upgrade
exit
ENDSCRIPT

cat > edit/tmp/cleanup.sh << ENDSCRIPT
# Cleanups
echo "" > /etc/resolv.conf
apt clean
apt purge --auto-remove -y
rm -rf /tmp/*
rm -rf /var/cache/apt-xapian-index/*
rm -rf /var/lib/apt/lists/*
rm -rf ~/.bash_history
umount /proc/sys/fs/binfmt_misc || true
umount /sys
umount /dev/pts
umount /proc
exit
ENDSCRIPT


chmod +x edit/tmp/*.sh
chroot edit ./tmp/prepare.sh
#If you want to test something in the chroot environment, then uncomment next srtings
#chroot edit
#exit

chroot edit ./tmp/custom.sh
chroot edit ./tmp/cleanup.sh
umount edit/dev

#--------------------
#---ISO generating---
#--------------------
#Manifest file
chmod +w extract-cd/casper/filesystem.manifest
chroot edit dpkg-query -W --showformat='${Package} ${Version}\n' > extract-cd/casper/filesystem.manifest
#chroot edit dpkg-query -W --showformat='${Package} ${Version}\n' > filesystem.manifest
#rm -f extract-cd/casper/filesystem.manifest
#mv filesystem.manifest extract-cd/casper
#sudo chown root:root extract-cd/casper/filesystem.manifest
chmod -w extract-cd/casper/filesystem.manifest

rm -f extract-cd/casper/filesystem.squashfs
#Default block size is 131072 bytes
mksquashfs edit extract-cd/casper/filesystem.squashfs
#Highest possible compression but it takes much more time
#mksquashfs edit extract-cd/casper/filesystem.squashfs -comp xz -e edit/boot

cd extract-cd
#Delete hashsums and generate the new with the sha256 algorithm
rm MD5SUMS
find -type f -print0 | sudo xargs -0 sha256sum | grep -v isolinux/boot.cat | sudo tee SHA256SUM

#Generate customized ISO-file
mkisofs -D -r -V "$NEW_NAME" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ../$NEW_NAME.iso .

#---Checksum generating---
cd ../
sha256sum $NEW_NAME.iso > $NEW_NAME.iso.sha256
#sha256sum -c $NEW_NAME.iso.sha256


#---Unmount and delete directories---
sudo umount edit/dev 
sudo umount edit/run
sudo umount edit/proc
sudo umount edit/sys
sudo umount mnt
sudo rm -rf extract-cd
sudo rm -rf mnt
sudo rm -rf edit

